
[TOC]

# ÜK M187 Portofolio


## Eintritt
- [Verzeichnis](./01_Eintritt/README.md)

## Hardware
- [Verzeichnis](./02_Hardware/README.md)

## Betriebssysteme
- [Verzeichnis](./03_Betriebssysteme/README.md)

## Speichergeräte
- [Verzeichnis](./04_Speichergeräte/README.md/)

## Benutzer und Gruppen
- [Verzeichnis](./05_Benutzer%20und%20Gruppen/README.md)

## Der Weg ins Internet
- [Verzeichnis](./06_Der%20Weg%20ins%20Internet/README.md)

## Fehlerbehebung
- [Verzeichnis](./07_Fehlerbehebung/README.md)

## Dienste
- [Verzeichnis](./08_Dienste/README.md)

## Arbeitsjournal
- [Arbeitsjournal](./Arbeitsjournal/187%20Arbeitsjournal.pdf)
# Eintrittstest

Wir haben einen Eintrittstest gemacht. Mein Ergebnis war 35 von 38 Punkten. Dies wiederrum sind ca 92% der Fragen, des Testes, welche ich richtig beantwortete. 

Die einzigen Fehler waren:
- Power USB statt **Micro USB**
- Die Schweiz aht nur **1** Tastaturlayout
- Ein GB sind **1'000'000'000 Bytes**

Den ganzen Test findet man hier ([Testergebnis](./Anlagen/Eintrittstest_%20Überprüfung%20des%20Testversuchs%20_%20Moodle%20_%20ZLI.pdf))
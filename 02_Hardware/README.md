[TOC]

# Hardware

## ESD

ESD steht für "Elektrostatische Entladung" (englisch: Electrostatic Discharge). ESD tritt auf, wenn sich elektrostatische Ladugen in einem Körper aufbauen und dann abrupt entladen, was zu einer kruzzeitigen hohen Spannung führen kann. 

ESD Warnbild:  
<img src="./Images/ESD.png" alt="drawing" width="100px"/>

ESD ist sehr gefährlich für die PC-Hardware. Wenn der unerwünschte, abrupte Transfer dieser elektrostatischen Spannung zwischen Personen oder Gegenstände und den empfindlichen Komponenten eines Computers stattfindet, können während der Montage, Reperatur oder Austausch, die Komponenten beschädigt werden.

Man kann sich zum Glück auf verschieden Arten vor ESD schützen:

- Auf einer Antistatischen Arbeitsmatte/Oberfläche arbeiten.
- Ein Antistatisches Armband verwenden.
- Das Metall des Gehäuses berühren um, sich zu entladen.
- ESD-sichere Kleidung tragen.
- Hardware in ihren Verpackungen lassen, bis kurz vor dem Einbau. 

## Werkstattauftrag 1

Wir mussten in einem Word-Dokument die Komponenten und die Schnitstellen analysieren und erklären. 

Link zum PDF -> [Werkstattauftrag 1](./Anlagen/Hardware.pdf)

## Komponenten

Ein Computer besteht aus vielen Bauteilen. Das sind die wichtigsten:

- **Mainboard (MB):** Das Mainboard ist die zentrale Platine, auf der andere Hardware-Komponenten wie CPU, RAM und Erweiterungskarten montiert sind. Es stellt die Verbindung zwischen den verschiedenen Teilen des Computers her.

- **Hauptprozessor (CPU):** Die CPU ist der Hauptprozessor des Computers und führt die meisten Berechnungen und Befehle aus. Sie ist das "Gehirn" des Systems.

- **Arbeitsspeicher (RAM):** Der RAM ist ein temporärer Speicher, der von der CPU für laufende Programme und Daten verwendet wird. Je mehr Grösse ein RAM hat desto  schneller sind die Rechenprozesse.

- **Festplatten (HDD/SSD):** Festplatten (HDDs) und Solid-State-Laufwerke (SSDs) sind Speichermedien für die langfristige Datenspeicherung. SSDs sind schneller und effizienter als HDDs.

- **Grafikprozessor (GPU):** Die GPU ist für die Verarbeitung von Grafiken und Bildern verantwortlich. Sie wird für Spiele, Videobearbeitung und grafikintensive Aufgaben eingesetzt.

- **Netzteil (PSU):** Das Netzteil versorgt den Computer mit elektrischer Energie. Es wandelt den Strom aus der Steckdose in die verschiedenen Spannungen um, die für die verschiedenen Komponenten benötigt werden.


## Mainboard (Hauptplatine)

Das Mainboard ist das zentrale Verbindungsstück eines Computers. Es beherbergt den CPU-Sockel, Steckplätze für RAM, Anschlüsse für Laufwerke und Slots für Erweiterungskarten (z.B. Grafik-, Sound- und Netzwerkkarten).
<img src="./Images/mainboard.png" alt="drawing" width="300px"/>

Bei Mainboards existieren definierte Größen, bekannt als "ATX"-Standard. PC-Gehäusehersteller orientieren sich an diesen Standards, um eine reibungslose Passform zu gewährleisten.
- E-ATX: 305 mm × 330 mm
- ATX: 305 mm × 244 mm
- XL-ATX: 345 mm × 262 mm
- ATX-Extended: 308 mm × 340 mm (Server Board Format)
- Mini-ATX: 284 mm × 208 mm
- Micro-ATX (µATX): 244 mm × 244 mm
- Flex-ATX: 229 mm × 191 mm
- Mini-ITX: 170 mm × 170 mm
- Nano-ITX: 120 mm × 120 mm
- Pico-ITX: 100 mm × 72 mm

<img src="./Images/Mainboardgrössen.png" alt="drawing" width="500px"/>

<img src="./Images/mainboard-masse.png" alt="drawing" width="300px"/>


Die frühere Northbridge ist nun in die CPU integriert, die "schnelle" Geräte wie CPU, RAM und Grafikkarte verbindet. Die Southbridge wurde durch den [PCH](https://en.wikipedia.org/wiki/Platform_Controller_Hub) (Platform Control Hub) ersetzt, der die "langsameren" Geräte mit dem Prozessor verbindet. 
<img src="./Images/blockdiagramm.png" alt="drawing" width="300px"/>
<img src="./Images/PCH.png" alt="drawing" width="300px"/>

BIOS und UEFI sind Firmware, die sich auf einem ROM auf dem Mainboard befindet. Obwohl UEFI den Begriff BIOS ersetzt hat, wird dieser immer noch häufig verwendet.

![UEFI](./Images/uefi.png)

Es gibt viele Interne Schnittstellen. Hier sind ein paar:

- **CPU-Sockel:** Verbindet die CPU mit dem Mainboard (z.B. AM4, LGA1151).

![AM4 Sockel](./Images/am4%20sockel.PNG) 
![Closed](./Images/Socket_1151_closed_01.jpg)

- **PCIe (x1-x16):** Werden verwendet um Grafikkarten, Natzwerkkarten oder andere Erweiterungskarten mit dem Mainboard zu verbinden.
Es gibt 4 verschiedene Grössen, Merksatz: "Je Datenintensiver ein Gerät um so breiter der Anschluss".
Mit PCI Version 4.0 werden Daten mit bis zu 2GB/s  (x1) |  8GB/s (x4) | 16GB/s (x8) |  32GB/s (x16) umgesetzt
![PCIe](./Images/pcie.png)

- **M.2:** Wird verwendet um SSDs der neuen Generation anzuschliessen.
Nutzt die PCIe übertragungskanäle und ist normalerweise direkt an die CPU angebunden.
![M.2](./Images/m.2.png)

- **SATA:** Wird verwendet um ältere Laufwerke (Festplatten, DVD-Laufwerke) der älteren Generation anzuschliessen.
Die Übertragungsrate hält sich bei ca. 600MB/s
![SATA](./Images/SATA.png)

- **RAM DDR Slots:** Die RAM Slots sind oftmals nummeriert oder und farblich getrennt. Werden diese richtig eingesetzt können Sie im Dual-Channel oder sogar Quad-Channel Modus arbeiten und so höhere Geschwindigkeiten erreichen.
![RAM Slots](./Images/RAM-Slot.png)

- **ATX (Stromversorgung):** Dient zur Stromversorgung des Mainboards.
Oft befindet sich direkt neben dem CPU Sockel ein 4 bzw. 8 Pin ATX für die Stromversorgung des CPU.
![ATX](./Images/ATX-Power.png)

Es gibt auch viele Schnittstellen. Hier sind ein paar:

- USB
- DP
- HDMI
- VGA
- RJ45
- 3.5mm Klinke
- PS/2

Weiteres dazu im Abschnitt "Schnittstellen".
![Schnittstellen](./Images/schnittstellen.jpg)

## Hauptprozessor

Aktuell dominieren AMD und Intel den Prozessormarkt. Intel bietet die bekannte "i"-Serie an, während AMD die "Ryzen"-Serie führt. Der Marktanteil von Intel beträgt 68.6% und der von AMD 31.4%.
![AMD VS Initel](./Images/AMD-Ryzen-vs-Intel.jpg)

Die CPU's werden in verschieden Grössen und Bauweisen ausgeliefert. 
Am stärksten zeigt sich der Unterschied bei den Pins auf der Unterseite. 

Mit dem Sockel bezeichnet man das Anschlussteil am Mainboard. Darin wird die CPU eingesetzt. 
Es gibt viele verschiedene Bauformen. Die CPU muss zwingend mit dem Sockel kompatibel sein, es ist nicht möglich Adapter einzusetzen. 

LGA (Land Grid Array) bedeutet, der CPU verfügt über keine PIN's. Man findet sie stattdessen auf dem Mainboard. Der Nachteil daran ist, dass diese sehr sensibel sind und sich schnell verbiegen lassen. Falls ein PIN verbogen ist, kann man sie praktisch nicht mehr zurückbiegen. Ist einer dieser PIN's abgebrochen,kann man das Mainboard nicht mehr benutzen. 
![LGA](./Images/LGA.jpg)

PGA (Pin Grid Array) bedeutet, die PIN's sind am CPU zu finden. Der CPU Sockel. weiste das Gegenstück der Verbindung auf. 
Der Nachteil, die CPU's müssen sehr vorsichtig behandelt werden. Sind die PIN's verbogen können diese nur schwer gerichtet werden. Falls ein PIN abbricht, kann es sein, dass die CPu nicht mehr funktioniert. 
![PGA](./Images/PGA.jpg)

Wenn der Prozessor einmal sitzt, muss die Kühlung gewährleistet werden. 
Wichtig hierbei ist der richtige Kühöer und die entsprechende Wärmeleitpaste. 
Man sollte beim Kauf der Paste unbedingt auf die Leitfähigkeit achten, je höher der Wert umso leitfähiger die Paste.
Paste mit schlechter Leitfähigkeit kann nämlich die Hitze stauen und somit den gegenteiligen effekt asulösen. 
[Wärmeleitpaste Wikipedia](https://de.wikipedia.org/wiki/W%C3%A4rmeleitpaste)

Geschwindigkeit sagt nicht immer alles aus. Eine CPU welche die Last auf 4x1GHz kerne verteilen kann ist oftmals schneller als eine CPU, welche die Last auf 2x2GHz verteilt.

Eine CPU funktioniert folendermassen:

- **Befehlsanholung (Fetch):** Die CPU liest Befehle aus dem RAM.
- **Dekodierung (Decode):** Die CPU übersetzt die Befehle in Steuerungssignale. 
- **Ausführung (Execute):** Die CPU verarbeitet diese Signale und führt die Befehle aus.
- **Speicher (Memory):** Bei Bedarf werden Daten zwischen den verschieden Speicherbereichen übertragen. 
- **Schreiben (Write):** Die Ergebnisse werden in den Speicher geschrieben, damit später darauf zugegriffen werden kann. 

Moderne CPU's haben mehrere Kerne, was bedeutet, dass sie mehrere Verarbeitunen gleichzeitig machen können. Somit läuft der Prozess viel schneller ab und der PC ist leistungsfähiger. 